import math  # ---------------------------------Math
from . import weights_creating  # ---------------------My file for creating weights
from . import NN_creating  # --------------------------My file for creating Neural Network Structure
import json  # ---------------------------------For file saving
import numpy as np

class NeuralNetwork:
	def __init__(self, neurons_placement, weight_load=0, already_learned=0, learning_rate=0.7, sigma='sigmoida'):
		self.neurons_placement = neurons_placement
		self.weight_load = weight_load
		self.already_learned = already_learned
		self.learning_rate = learning_rate
		self.sigma = sigma
		if self.weight_load == 0:  # -------------------------------------- 0 is Create new Network
			self.weights = weights_creating.weight(self.neurons_placement)
		else:  # ---------------------------------Download inputed in NN_image_setup.py weights values
			self.weights = self.weight_load

		# ----------------------------------------Create table of: Neurons, target_outputs, Gradients
		self.neurons, self.err, self.gradient = NN_creating.network(self.neurons_placement)


	def network_reset(self):
		for i in range(len(self.err)):
			self.err[i] = np.zeros_like(self.err[i])

		for i in range(len(self.gradient)):
			self.gradient[i] = np.zeros_like(self.gradient[i])

		for i in range(len(self.neurons)):
			self.neurons[i] = np.zeros_like(self.neurons[i])
			if i != len(self.neurons) -1:
				self.neurons[i][-1] = 1

		
	def forward(self, input):
		for i in range(len(self.neurons[0]) - 1):
			self.neurons[0][i] = input[i]

		for i in range(1, len(self.neurons)):
			if i == len(self.neurons)-1:
				for out in range(len(self.neurons[i])):
					self.neurons[i][out] = np.dot(self.neurons[i - 1], self.weights[i - 1][out])
					self.neurons[i][out] = self.activation(self.neurons[i][out])
			else:
				for out in range(len(self.neurons[i])-1):
					self.neurons[i][out] = np.dot(self.neurons[i - 1], self.weights[i - 1][out])
					self.neurons[i][out] = self.activation(self.neurons[i][out])
			
	def backward(self, target_output):
		self.err[-1] = np.subtract(target_output, self.neurons[-1])

		for i in range(len(self.weights) - 1, -1, -1):

			temp2 = np.zeros(len(self.neurons[i+1]))
			for out in range(len(self.neurons[i+1])):
				temp2[out] = self.psig(self.neurons[i+1][out])

			self.gradient[i] = np.multiply(temp2, np.dot(self.learning_rate, self.err[i]))
			for out in range(len(self.weights[i])):
				self.weights[i][out] += np.dot(self.gradient[i][out], self.neurons[i])
				if i != 0:
					self.err[i - 1] += np.dot(self.err[i][out], self.weights[i][out])
		return self.weights

	def train(self, input, target_output):
		#print("Input has to be beetwen 0 and 1. It's up to you to normalize, also give me target_output, a list")
		self.network_reset()
		self.forward(input)
		self.backward(target_output)

	def total(self, target_output):
		temp = 0
		for i in range(len(self.err[-1])):
			temp += 1 / 2 * ((target_output[i] - self.err[-1][i]) ** 2)
		return temp

	def activation(self, x):
		if self.sigma == 'sigmoida':
			return 1 / (1 + math.exp(-x))

		elif self.sigma == 'tan':
			#return (math.exp(2*x)-1)/(math.exp(2*x)+1)
			return (2/(1 + math.exp(-2*x))) - 1
			

	def psig(self, x):
		if self.sigma == 'sigmoida':
			return x * (1 - x)

		elif self.sigma == 'tan':
			#return (2/(math.exp(x) + math.exp(-x)))**2
			#return 1 - ((2/(1 + math.exp(-2*x))) - 1)**2
			return 1 - self.activation(x)**2
#1 - ((2/(1 + e^(-2*x))) - 1)^2
#1 - (2/(1 + e^(-2*x) - 1))^2
   
	def save(self, repeats, dir=''):
		np.save(dir + f"{self.neurons_placement}_{repeats}_weights", np.array(self.weights, dtype=np.ndarray))

