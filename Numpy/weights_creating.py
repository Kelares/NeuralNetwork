from random import randint, random
import numpy as np

def weight(neurons_placement):
    weights = []
    for i in range(len(neurons_placement) - 1):
        weights.append([])
        for j in range(neurons_placement[i + 1]):
            weights[i].append(np.random.default_rng().random(neurons_placement[i] + 1))
    # print(weights[0][0])
    # for i in range(len(weights)):
    #     for j in range(len(weights[i])):
    #         r = randint(0,1)
    #         if r:
    #             weights[i][j] *= -1
    return weights
