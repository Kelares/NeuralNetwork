import numpy as np
def network(neuron_len):
    neurons = []
    for i in range(len(neuron_len)):  # every neuron layer
        if i != len(neuron_len) - 1:
           neurons.append(np.zeros(neuron_len[i] +1))
        else:
           neurons.append(np.zeros(neuron_len[i]))

    for i in range(len(neuron_len) - 1):
        neurons[i][-1] = 1

    gradient = []
    err = []

    for i in range(len(neuron_len) - 1):
        if i != len(neuron_len) - 2:
            gradient.append(np.zeros(neuron_len[i+1] +1))
            err.append(np.zeros(neuron_len[i+1] +1))
        else:
            gradient.append(np.zeros(neuron_len[i+1]))
            err.append(np.zeros(neuron_len[i+1]))

    return neurons, gradient, err
