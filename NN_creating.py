def network(neuron_len):
    neurons = []
    for i in range(len(neuron_len)):  # every neuron layer
        neurons.append([])
        if i != len(neuron_len) - 1:
            for j in range(neuron_len[i] + 1):
                neurons[i].append(0)
        else:
            for j in range(neuron_len[i]):
                neurons[i].append(0)

    for i in range(len(neuron_len) - 1):
        neurons[i][-1] = 1

    gradient = []  # gradient
    err = []
    for i in range(len(neuron_len) - 1):
        gradient.append([])
        err.append([])
        if i != len(neuron_len) - 2:
            for j in range(neuron_len[i + 1] + 1):
                gradient[i].append(0)
                err[i].append(0)
        else:
            for j in range(neuron_len[i + 1]):
                gradient[i].append(0)
                err[i].append(0)

    # print("NN_creating")
    return neurons, err, gradient
