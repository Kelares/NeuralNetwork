from random import randint, random


def weight(neuron_len):
    weights = []
    for i in range(len(neuron_len) - 1):
        weights.append([])
        if i != len(neuron_len):
            for j in range(neuron_len[i] + 1):
                weights[i].append([])
        else:
            for j in range(neuron_len[i]):
                weights[i].append([])

    for i in range(len(neuron_len) - 1):
        if i == 0:
            for inp in range(neuron_len[i] + 1):
                for out in range(neuron_len[i + 1] + 1):
                    qq = randint(0, 1)
                    if qq == 0:
                        r = random() * -1
                    else:
                        r = random()
                    weights[i][inp].append(r)

        if 0 < i < len(neuron_len) - 2:
            for inp in range(neuron_len[i] + 1):
                for out in range(neuron_len[i + 1] + 1):
                    qq = randint(0, 1)
                    if qq == 0:
                        r = random() * -1
                    else:
                        r = random()
                    weights[i][inp].append(r)

        if i == len(neuron_len) - 2:
            for inp in range(neuron_len[i] + 1):
                for out in range(neuron_len[i + 1]):
                    qq = randint(0, 1)
                    if qq == 0:
                        r = random() * -1
                    else:
                        r = random()
                    weights[i][inp].append(r)

    return weights
